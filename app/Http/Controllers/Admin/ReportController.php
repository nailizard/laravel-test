<?php

namespace App\Http\Controllers\Admin;

use App\Enums\ReportTypes;
use App\Http\Controllers\Controller;
use App\Jobs\GenerateCsvForModel;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected array $supportedTypes = [
        'users' => ReportTypes::USERS,
    ];

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function __invoke(Request $request, string $type)
    {
        if (!array_key_exists($type, $this->supportedTypes)) {
            return response()->json(['message' => "Report type not supported", 'type' => 'error'], 422);
        }

        $this->dispatch(new GenerateCsvForModel($type));

        return response(null);
    }
}
