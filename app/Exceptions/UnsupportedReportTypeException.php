<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class UnsupportedReportTypeException extends Exception
{
    public function __construct($message = "Report type unsupported", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
