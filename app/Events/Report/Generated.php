<?php

namespace App\Events\Report;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class Generated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $path;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }
}
