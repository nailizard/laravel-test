<?php

namespace App\Jobs;

use App\Enums\ReportTypes;
use App\Events\Report\Generated;
use App\Exceptions\UnsupportedReportTypeException;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\SimpleExcel\SimpleExcelWriter;

class GenerateCsvForModel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $type;

    public array $types = [
        'users' => ReportTypes::USERS,
    ];

    public string $filePath;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws \Throwable
     */
    public function __construct(string $model)
    {
        if (!array_key_exists($model, $this->types)) {
            throw new UnsupportedReportTypeException();
        }

        $this->type = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        try {
            $model = new $this->types[$this->type]();

            $reportName = $model->getTable();

            $date = Carbon::now()->unix();

            $this->filePath = storage_path("app/{$reportName}-report-${date}.csv");

            $users = $model->withTrashed()->cursor();

            $writer = SimpleExcelWriter::create($this->filePath, 'csv');

            foreach ($users as $user) {
                $writer->addRow($user->toArray());
            }

            $writer->close();

            event(new Generated($this->filePath));

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
