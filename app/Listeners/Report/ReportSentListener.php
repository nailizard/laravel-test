<?php

namespace App\Listeners\Report;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ReportSentListener
{
    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        if ($event->message->getSubject() === 'User Report Generated') {
            $path = $event->data['path'] ?? null;
            if ($path && file_exists($path)) {
                unlink($path);
            }
        }
    }
}
