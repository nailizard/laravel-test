<?php

namespace App\Listeners\Report;

use App\Events\Report\Generated;
use App\Mail\UserReportGenerated;
use App\Models\Auth\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class GeneratedListener
{
    /**
     * Handle the event.
     *
     * @param  Generated  $event
     * @return void
     */
    public function handle(Generated $event)
    {
        // this is naive, assumes first admin only
        $admin = User::admin()->first();
        if ($admin) {
            Mail::to($admin)->send(new UserReportGenerated($event->path));
        }
    }
}
