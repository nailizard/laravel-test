@component('mail::message')
# User report

Please find the user report attached.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
