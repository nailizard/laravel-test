<?php

namespace Tests\Feature;

use App\Jobs\GenerateCsvForModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Str;
use Tests\TestCase;

class GenerateUserCsvTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    protected string $reportType = 'users';

    /** @test */
    public function admin_reports_endpoint_fires_generate_report_job()
    {
        Bus::fake();

        $this->get("/admin/reports/{$this->reportType}")
            ->assertStatus(200);

        Bus::assertDispatched(function (GenerateCsvForModel $job) {
            return Str::contains($job->type, $this->reportType);
        });
    }

    /** @test */
    public function fails_for_unsupported_report_type()
    {
        $reportType = "unsupported";

        $this->get("/admin/reports/{$reportType}")
            ->assertStatus(422)
            ->assertJson(['message' => "Report type not supported", 'type' => 'error']);
    }
}
